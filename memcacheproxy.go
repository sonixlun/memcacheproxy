package main

import (
	"bytes"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/go-redis/redis"
)

// Reader - provides the read() method
type Reader interface {
	read(key string) []byte
}

var (
	templateReader Reader
	valueReader    Reader
	config         Config
	served         int64
)

// Config - provides a structure for the application configuration
type Config struct {
	Storage struct {
		Templates Storage `json:"templates"`
		Values    Storage `json:"values"`
	} `json:"storage"`
	Server struct {
		Port string `json:"port"`
	} `json:"http_server"`
	Routes []Route `json:"routes"`
}

// Storage - is the storage structure part of the configuration file
type Storage struct {
	Type string `json:"type"`
	Host string `json:"host"`
}

// Route - is the route structure part of the configuration file
type Route struct {
	Route    string        `json:"route"`
	Ttl      time.Duration `json:"ttl"`
	Template string        `json:"template"`
	Value    string        `json:"value"`
	Buf      *(bytes.Buffer)
	Locked   bool
}

// MemcacheReader - will implement read() for Reader interface
type MemcacheReader struct {
	Client *memcache.Client
}

// RedisReader - will implement read() for Reader interface
type RedisReader struct {
	Client *redis.Client
}

// FileReader - will implement read() for Reader interface
type FileReader struct {
}

// read - implements read for memcache
func (r MemcacheReader) read(key string) []byte {
	val, err := r.Client.Get(key)

	if err != nil {
		println(err.Error())
		println(val.Value)
		return nil
	}

	return val.Value
}

// read - implements read for redis
func (r RedisReader) read(key string) []byte {
	return []byte(r.Client.Get(key).Val())
}

// read - implements read for file
func (r FileReader) read(key string) []byte {
	val, err := ioutil.ReadFile(key)

	if err != nil {
		println(err.Error())
		println(val)
		return nil
	}

	return val
}

// main - app entry point
func main() {
	config = LoadConfiguration()
	served = 0

	// Only for testing/development ENV
	// TODO: remove/comment this on any production/staging ENV
	writeTestData()

	// For each route: (1) - prepare the http content; (2) - update the http content; (3) - serve the http content
	for indx := range config.Routes {

		route := &config.Routes[indx]
		route.Buf = new(bytes.Buffer)

		// Build the template and start a ticker to populate it with updated data every ruta.Ttl miliseconds
		go func(ruta *Route) {
			var templ = getTemplate(ruta) // load route template
			var ticker = time.NewTicker(ruta.Ttl * time.Millisecond)
			ruta.Locked = false

			defer ticker.Stop()

			// ticker for getting updated value from cache
			for range ticker.C {
				if ruta.Locked {
					continue
				}

				var data map[string]interface{}
				// Read the updated value
				val := valueReader.read(ruta.Value)

				// Decode the JSON value (always a JSON value is expected) from cache
				if err := json.Unmarshal(val, &data); err != nil {
					println("Error reading JSON value from storage")
					continue
				}

				// Lock the route responses until we update (miliseconds)
				ruta.Locked = true
				// Empty the current content value
				ruta.Buf.Reset()

				// rebuild the http "static" response with the new loaded value
				if err := templ.Execute(ruta.Buf, data); err != nil {
					println(err.Error())

					// Unlock the route response after the update
					ruta.Locked = false
				}

				ruta.Locked = false
			}
		}(route)

		// Add the route handler - write the buf value in the response.
		http.HandleFunc(route.Route, func(w http.ResponseWriter, r *http.Request) {
			// if the response is in the process of being rebuilt then wait for a bit to avoid partial responses
			for route.Locked {
				time.Sleep(20 * time.Millisecond)
			}
			w.Write(route.Buf.Bytes())
			served++
		})
	}

	//Report requests per second on the http routes
	go func() {
		var servedTicker = time.NewTicker(time.Second)

		defer servedTicker.Stop()

		for range servedTicker.C {
			t := time.Now()

			if served > 0 {
				println(t.Format(time.RFC3339), ": Serving: ", served, " req/s")
				served = 0
			}
		}
	}()

	// Start the http server on the provided port
	http.ListenAndServe(":"+config.Server.Port, nil)
}

//LoadConfiguration - Load application configuration from the config.json file
func LoadConfiguration() Config {
	var config Config
	configFile, err := os.Open("config.json")
	defer configFile.Close()
	if err != nil {
		println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)

	templateReader = getReader(config.Storage.Templates)

	if templateReader == nil {
		println("Error creating the templates reader.")
	}

	valueReader = getReader(config.Storage.Values)

	if valueReader == nil {
		println("Error creating the values reader.")
	}
	return config
}

//getReader - Returns the Reader interface
func getReader(storage Storage) Reader {

	if storage.Type == "memcache" {
		println("Loading memcache reader")
		return MemcacheReader{Client: memcache.New(storage.Host)}
	}

	if storage.Type == "redis" {
		println("Loading redis reader")
		return RedisReader{Client: redis.NewClient(&redis.Options{
			Addr:     storage.Host,
			Password: "", // no password
			DB:       0,  // default DB
		})}
	}

	if storage.Type == "file" {
		println("Loading file reader")
		return FileReader{}
	}

	return nil
}

//getTemplate - returns a pointer to the html template object
func getTemplate(route *Route) *template.Template {
	templ := templateReader.read(route.Template)
	t, err := template.New("foo").Parse(string(templ))

	if err != nil {
		return nil
	}

	return t
}

//writeTestData - used only for testing purposes - populates memcache with some test data
func writeTestData() {
	jsData, err := ioutil.ReadFile("input.json")

	if err != nil {
		println("Error loading json file.")
	}
	mc := memcache.New(config.Storage.Values.Host)

	// Write data in memcache - for testing purposes only
	mc.Set(&memcache.Item{
		Key:        "foo",
		Value:      jsData,
		Expiration: 0})
}
