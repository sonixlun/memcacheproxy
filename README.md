Steps:
-----------------
1. Edit config.json file to add/remove/alter routes/memcache settings/web server settings
2. [optional] Build the native executable
3. Run


Config file:
-----------------
Looks like this:

```json
{
	"routes": [
		{
			"route": "/",
			"ttl": 2000,
			"template": "templates/template.htm",
			"value": "foo"
		},
		{
			"route": "/dd/",
			"ttl": 3000,
			"template": "templates/template1.htm",
			"value": "foo"
		}
	],
	"storage": {
		"templates" : {
			"type": "file",
			"host": ""
		},
		"values": {
			"type": "memcache",
			"host": "localhost:11211"
		}
	},
	"http_server": {
		"port": "4000"
	}
}
```

Build static:
-----------------
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' .


Build:
-----------------
go build memcacheproxy.go


Use:
-----------------
./memcacheproxy

or (if it was not built before)

go run memcacheproxy.go


Performance:
-----------------

<pre>wrk -c 100 -t 3 http://127.0.0.1:4000/dd/ --latency
Running 10s test @ http://127.0.0.1:4000/dd/
  3 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.14ms    1.68ms  43.72ms   90.26%
    Req/Sec    42.99k     4.85k   58.18k    70.33%
  Latency Distribution
     50%  575.00us
     75%    1.31ms
     90%    2.77ms
     99%    8.46ms
  1288670 requests in 10.04s, 4.50GB read
Requests/sec: 128303.45
Transfer/sec:    458.73MB
</pre>